angular.module('starter.controllers', ['ti-segmented-control'])

.run(function($http, $state, $rootScope, $location) {

$rootScope.go_back = function() {
  $location.path("tab.home");
};

$rootScope.go_choose = function() {
	$location.path("choose");
};

$rootScope.user_get_project = function($user) {
$http.get("http://188.166.175.41/mon-quotidien/search_project_for_user.php/?user_mail="+$user)
        .then(function(response) {
		console.log(response.data);
		if (strncmp(response.data, "0", 1) != 0)
		{
			var str = String(response.data);
			$rootScope.projects = str.split("\n");
			$rootScope.projects.pop();
		}
        });
}

$rootScope.how_many_line = function() {
$http.get("http://188.166.175.41/mon-quotidien/how_many_line.php/?id="+$rootScope.sel+"&name="+$rootScope.project)
        .then(function(response) {
		console.log(response.data);
		$rootScope.number = response.data;
        });
}

$rootScope.get_all_user = function() {
$http.get("http://188.166.175.41/mon-quotidien/get_user_to_project.php/?name="+$rootScope.project)
        .then(function(response) {
		console.log(response.data);
		if (strncmp(response.data, "0", 1) != 0)
		{
			var str = String(response.data);
			$rootScope.userauth = str.split("\n");
			$rootScope.userauth.pop();
			for(var i = 0; i < $rootScope.userauth.length; i++)
			{
				if ($rootScope.userauth[i] == $rootScope.user)
				{
					$rootScope.userauth.splice(i, 1);
					break;
				}
			}
		}
        });
}
$rootScope.create_graphe_json = function(ser, dat, lab) {
    var format = {
        series: ser,
        data: dat,
        labels: lab,
        //CHANGE COLOR IF NEEDED;
        colours: ['#F2DD676','#3C68BA','#FD1F5E']
    };
    return (format);
}

$rootScope.init_data = function($data)
{
    $rootScope.current_data = [];
    for (var i = 0; i < $data.qas.length; i++)
    {
       $rootScope.current_data[i] = { type: $data.qas[i].a.id, answer: (($data.qas[i].a.id == 4) ? "" : 0 ) };
    }
}

$rootScope.init_chart = function($data)
{
    $rootScope.charts_yna = [{yes: 0, no: 0, accid: 0, qa: ""}];
    var a = 0;
    for (var i = 0; i < $data.all_datas.length; i++)
    {
        a = 0;
        for (var y = 0; y < $data.all_datas[i].datas.length; y++)
        {
            if ($data.all_datas[i].datas[y].type == '1')
            {
                $rootScope.charts_yna[a].yes += (($data.all_datas[i].datas[y].answer == 0) ? 1: 0);
                $rootScope.charts_yna[a].no += (($data.all_datas[i].datas[y].answer == 1) ? 1: 0);
                $rootScope.charts_yna[a].accid += (($data.all_datas[i].datas[y].answer == 2) ? 1: 0);
                $rootScope.charts_yna[a].qa = $rootScope.protos[$rootScope.sel].qas[y].q;
                a++;
                if (a >= $rootScope.charts_yna.length)
                   $rootScope.charts_yna.push({yes: 0, no: 0, accid: 0, qa: ""});
            }
        }
    }
    $rootScope.value_yna = [];
    $rootScope.labels_yna = ["oui", "non", "accident"];
    console.log($rootScope.charts_yna);
    for (var i = 0; i < $rootScope.charts_yna.length; i++)
    {
        $rootScope.value_yna[i] = [$rootScope.charts_yna[i].yes, $rootScope.charts_yna[i].no, $rootScope.charts_yna[i].acci];
    }
    $rootScope.pie = [];
    for(var i = 0; i < $rootScope.charts_yna.length ; i++)
    {
        $rootScope.pie[i] = $rootScope.create_graphe_json(0, [$rootScope.charts_yna[i].yes, $rootScope.charts_yna[i].no, $rootScope.charts_yna[i].accid], ['yes', 'non', 'accident']);
    }
}
$rootScope.user = "";
$rootScope.userauth = [];
$rootScope.project = "";

$rootScope.get_protos = function()
{
	var name = $rootScope.project;
	$http.get("http://188.166.175.41/mon-quotidien/get_protos.php/?email="+
		$rootScope.user+"&name="+name)
        .then(function(response) {
		console.log(response.data);
		if (strncmp(response.data, "ERROR", 5) != 0)
		{
			$rootScope.protos = [];
			var a = 0;
			var tab = String(response.data).split("\n");
			for (var i = 0; i < tab.length - 1; i += 3)
			{
				var ti = tab[i].split(",");
				var tmp_cat = {title: ti[0],
				logo: ti[1] };
				var tq = tab[i + 1].split(";");
				var ta = tab[i + 2].split(";");
				var tmp = [];
				for (var j = 0; j < tq.length; j++)
				{
					tmp.push({q: tq[j], a: {id: String(ta[j]) }});
				}
				$rootScope.protos.push({ categorie: tmp_cat, qas: tmp, creation_time: ti[2] });
				a++;
			}
			console.log($rootScope.protos);
		}
	});
}

//I make you an example, it's an array of a object contaning the categorie choose ande the qas (questions and reponses)
 $rootScope.projects = [];
    $rootScope.choose = 0;
   $rootScope.categories = [
    {
        title: "manger",
        logo: "http://www.westbaptist.org.nz/wp/wp-content/uploads/2010/03/eat-icon.jpg"
    },
    {
      title: "hygiene",
      logo: "http://partnermed.byhirvi.no/images/modules/icon_hygiene.png"
    },
    {
        title: "dormir",
        logo: "https://wfcastle33.files.wordpress.com/2014/03/icon-sleep.png"
    },
    {
        title: "sport",
        logo: "http://www.dreamix-studio.com/runtrainer/runtrainer_icon.png"
    },
    {
        title: "devoir maison",
        logo: "http://www.iconshock.com/img_jpg/XMac/education_icons/jpg/256/homework_icon.jpg"
    },
    {
        title: "comportement",
        logo: "https://www.seemescotland.org/media/1041/icon_mission_behavior_200.png?anchor=center&mode=crop&quality=90&width=160&heightratio=1&slimmage=true&rnd=130574217830000000"
    }
    ];
 $rootScope.protos = [
          //element 1
          {
          categorie: {title: "example", logo: "http://vignette2.wikia.nocookie.net/fantendo/images/5/5b/Color_wheel_dock_icon_by_andybaumgar-d3ezjgc.png/revision/latest?cb=20141006223915"},
          qas: [
            {q: "date/heure", a:  {id: '3', name: 'date'} },
             {q: "avec quel adulte est il parti au toilette?", a:  {id: '7', name: 'qui'} },
              {q: "a t'il fait pipi?", a:  {id: '1', name: 'oui/non/accident'} },
               {q: "a t'il utilise la Selle?", a:  {id: '1', name: 'oui/non/accident'} },
                 {q: "est-ce une demande spontanee?", a:  {id: '6', name: 'oui/non/accident'} },
            {q: "Remarque", a:  {id: '4', name: 'oui/non/accident'} }
              ],
          creation_time: 0 //date of creation of this proto (not important now)
          }
        ];
        $rootScope.saved = [
            //FOR 1 CATEGORIE (here example)
            {
                all_datas: [{
                        datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 0}],
                        creation_time: 0
                },
                {
                        datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 1}],
                        creation_time: 0
                },
                {
                        datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 2}],
                        creation_time: 0
                },
                {
                        datas: [{type: '3', answer: 0}, {type: '7', answer: 0}, {type: '1', answer: 0}],
                        creation_time: 0
                }
                ],
                categorie_id: "example"
            }
            ];
	
if ($rootScope.user == "")
	$location.path("log");
});
