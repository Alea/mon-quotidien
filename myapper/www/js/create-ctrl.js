angular.module('starter.controllers')

.controller('CreateCategorieCtrl', function($rootScope, $scope, $ionicPopup, $timeout, $location, $ionicNavBarDelegate) {
 
$scope.open_champ = function($index) {
  $rootScope.choose = $index;
  $location.path("cp");
}

$scope.add_champ = function() {
$scope.data = {};
  var myPopup = $ionicPopup.show({
    template: '<input type="text" ng-model="data.new">',
    title: 'Ajouter votre nouvelle categories',
    subTitle: 'Vous pourrez modifier le logo plus tard',
    scope: $scope,
    buttons: [{ text: 'Cancel' }, {text: '<b>Save</b>', type: 'button-royal', onTap: function(e) {if (!$scope.data.new) {
          } else {$rootScope.categories.push({title: $scope.data.new, logo:"http://www.iboxsolutions.com/wp-content/uploads/2014/11/icon-page-project-management.png"});
    return $scope.data.new;}}}]});
  myPopup.then(function(res) {
    console.log('Tapped!', res);
  });
};

})

.controller('CreateProtoCtrl', function($rootScope, $scope, $location, $http,  $ionicNavBarDelegate){
  $scope.nb_question = 1;
  $scope.categorie_choose = $rootScope.categories[$rootScope.choose].title;

  $scope.items = [
      {id: '1', name: 'oui/non/accident'},
     /* {id: '2', name: 'image'}, */
      {id: '3', name: 'date'},
       {id: '4', name: 'remarque'},
        /*{id: '5', name: 'dessin'}, */
        {id: '6', name: 'oui/non'},
 /*       {id: '7', name: 'qui'},*/
        {id: '8', name: 'compteur'}
    ];

$scope.data = [{ q: "", a: {id: '1', name: 'oui/non/accident'} }];

    $scope.go_categorie = function() {
	$location.path("categorie");
  
}
  $scope.add_qa = function() {
    $scope.data.push({ q: "", a: {id: '1', name: 'oui/non/accident'} });
    };
   $scope.create_proto = function() {
	var actual = new Date();
	$http.get("http://188.166.175.41/mon-quotidien/create_protos.php/?email="+$rootScope.user+"&name_id="+$rootScope.protos.length+"&name="+$rootScope.project+"&name_cat="+$rootScope.categories[$rootScope.choose].title+"&logo_cat="+$rootScope.categories[$rootScope.choose].logo+"&time="+String(actual.getTime())+"&protos="+JSON.stringify($scope.data))
        .then(function(response) {
		console.log(response.data);
		if (strncmp(response.data, "OK", 2) == 0)
		{
			console.log("created succesfully");
      			$location.path("go");
		}
	});
      $rootScope.saved.push({ all_datas: [], categorie_id: $rootScope.categories[$rootScope.choose]});
$rootScope.protos.push(
      {
          categorie: $rootScope.categories[$rootScope.choose],
          qas: $scope.data,
          creation_time: new Date()
      }
      );
	$location.path("tab.home");
 };
    $scope.del_qa = function($index) {
if ($index > 0) {$scope.data.splice($index, 1);}
};
})

;
